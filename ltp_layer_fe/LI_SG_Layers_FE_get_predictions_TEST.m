% Test LI_SG_Layers_FE_get_predictions 
% LI_SG_Layers_FE_get_predictions_TEST requires Matlab R2016a
% LI_SG_Layers_FE_get_predictions must be compatible with Matlab R2013a

clear all 

%%
datafolder = '..\..\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_ONE_flock'
%{
Params available in LI_SG:
       'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param,
%}

% Aliases for (frequently used) fields in data
PAR = 'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param
NORM = 'FE1';
EP = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00';
EP_NORM = 'EP';
AGE = 'age_days';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

data = PRODs2table(datafolder,AGE,{PAR,NORM,EP,EP_NORM},GROUP,CLIENT,DESCRIPTOR);

%% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
num_units = numel(group_name);

Fnc_handles = LI_SG_Layers_FE_get_predictions();
model_struct = load('FE_layer_all_test.mat');
model = LTPModel.from_struct(model_struct);
age_today = 250;

for k = 1:num_units
    flock = group==k;
    % FE 
    rnd_flock_days   = data.(AGE)(flock);
    rnd_flock        = data.(PAR)(flock);
    ynorm_days       = data.(AGE)(flock);
    ynorm            = data.(NORM)(flock);
    % EP
    EP_rnd_flock_days= data.(AGE)(flock);
    EP_rnd_flock     = data.(EP)(flock);
    ynorm_ep_days    = data.(AGE)(flock);
    ynorm_ep         = data.(EP_NORM)(flock);
    
    [ final_matrix, output, prediction_method, final_matrix_vars ] = Fnc_handles.main( rnd_flock, rnd_flock_days, EP_rnd_flock, EP_rnd_flock_days, ynorm, ynorm_days, ynorm_ep, ynorm_ep_days, model);
end