function plot_layer_norms()
% Load norms from xls file with multiple tabs
% Make sure that the xls file has set decimal separator = '.', and
% thousands separator = ',' (file/Options/Advanced tab)

filename_norms = '..\data\Layer.xlsx'
ppt_filename = 'layer_norms.pptx';

%% Start new presentation
try
    isOpen = exportToPPTX();
catch % add exportToPPTX toolbox to path
    addpath('exportToPPTX');
    isOpen = exportToPPTX();
end % try
    
if ~isempty(isOpen),
    % If PowerPoint already started, then close first and then open a new one
    exportToPPTX('close');
end
try
    exportToPPTX('open',fileName_str);
catch
    exportToPPTX('new','Dimensions',[11 8.5]);
end

%% loop over all tab in xlsx
[~,norm_codes] = xlsfinfo(filename_norms);
for k = 1:numel(norm_codes)
    norm_code = norm_codes{k};
    [~,~,raw] = xlsread(filename_norms,norm_code);
    pos_Days = strcmpi('Days',raw(1,:));
    pos_FE   = strcmpi('FE',raw(1,:));
    if ~any(pos_FE)
        disp('No FE var. in:')
        disp(raw(1,:))
    end
    Days = cell2mat(raw(2:end,pos_Days));
    FE = cell2mat(raw(2:end,pos_FE));
    if all(isnan(FE))
        continue
    end
    % plot FE norm
    h_fig = figure;
    plot(Days,FE,'ro-')
    title(norm_code,'Interpreter','none')
    xlabel('Days')
    ylabel('FE norm (g)')
    
    % save to ppt
    slideNum = exportToPPTX('addslide');
    fprintf('Added slide %d\n',slideNum);
    exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
    exportToPPTX('addtext',['Norm ' norm_code]);
    exportToPPTX('addnote','');
end

%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
newFile = exportToPPTX('saveandclose',ppt_filename);

end