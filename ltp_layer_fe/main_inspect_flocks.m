% MAIN_INSPECT_FLOCKS
% 1. Find duplicate flock IDs
%
% INPUT:
% 	None explicitly.
%   Go to get_data() to select source data folder and parameters.

%% Main
function main_inspect_flocks()
output_file = 'exclude_files.txt';

[data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_data();

% Find Prod files with duplicate IDs
duplicate_id = {}
for id = unique(data.(GROUP))'
    pos = strcmp(id,data.(GROUP));
    files = unique(data.filename(pos,:));
    if length(files) > 1
        duplicate_id{end+1} = files;
    end
end

% For each set of duplicate flock IDs, find files with least data for a give
% parameter.
for i = 1:length(duplicate_id)
    files = duplicate_id{i}
    for j = 1:length(files)
        f = files{j};
        pos = strcmp(f,data.filename);
        y = data.(PAR)(pos);
        duplicate_id_dataCount(i,j) = sum(~isnan(y));
    end
    [~,imax] = max(duplicate_id_dataCount(i,:));
    duplicate_files_to_remove{i} = files(setdiff(1:end,imax))  
end
duplicate_files_to_remove = [duplicate_files_to_remove{:,:}]';
fid = fopen(output_file, 'a+');
fprintf(fid,'%s\n\n','------- Duplicated ID files to be removed, with least data -------------');
cellfun(@(f) fprintf(fid,'%s\n',f),duplicate_files_to_remove);
fclose(fid);

% For each fileFor the selected PAR, check flock properties in each file,
% find files that do not pass the check
% Find Prod files with duplicate IDs
fid = fopen(output_file, 'a+');
fprintf(fid,'\n\n%s\n\n','------- Manually selected files for removal -------------');
fclose(fid);
files = unique(data.filename);
for i = 310:length(files)  %1:length(files)
    disp(['File # ' num2str(i) ' / ' num2str(length(files))])
    file = files{i}
    pos = strcmp(file,data.filename);
    y = data.(PAR)(pos);
    x = data.(AGE)(pos);
    yn = data.(NORM)(pos);
    fh = figure;
    plot(x,yn,'go-',x,y,'r*-')
    legend('Norm','Data')
    response = input('Exclude file? y/n: ','s');
    if strcmpi(response,'y') 
        
        fid = fopen(output_file, 'a+');
        fprintf(fid,'%d\t%s\n',i,file);
        fclose(fid);
    end
    close(fh)
end
end

%% Get data
function [data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR, datafolder] = get_data()
% GET_TRAINING_DATA reads PROD files and returns table with selected
% parameters, grouping variable, and aliases for frequently used fields in data.

datafolder = '..\Data\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_subset_test'
datafolder = '..\Data\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered'

% Aliases for (frequently used) fields in data
PAR = 'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param
NORM = 'FE1';
PAR_DISPLAY_NAME = PAR;
EP = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00';
EP_NORM = 'EP';
AGE = 'age_days';
AGE_DISPLAY_NAME = 'Age (days)';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

data = PRODs2table(datafolder,AGE,{PAR,NORM,EP,EP_NORM},GROUP,CLIENT,DESCRIPTOR);

end