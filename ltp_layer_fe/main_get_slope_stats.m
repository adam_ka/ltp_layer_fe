function main_get_slope_stats()
% Examine if FE trend (up, down, flat) correlates with client and norm
% tested with MATLAB R2016a

%{
4 x arv: the same flock id as jhenwnoorlander
arv - NaNs in FE and EP
"C:\Users\adamk\work\ltp\layer\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_NoDuplicateID\arv_noorlander_2boven_201405_6ad1e64d-9941-4680-875c-d23022addb78_Prod.json"
"C:\Users\adamk\work\ltp\layer\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_NoDuplicateID\arv_noorlander_2boven_201405_6ad1e64d-9941-4680-875c-d23022addb78_Prod.txt"
"C:\Users\adamk\work\ltp\layer\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_NoDuplicateID\jhenwnoorlander_jhenwnoorlander_2onder_201405_6ad1e64d-9941-4680-875c-d23022addb78_Prod.json"
"C:\Users\adamk\work\ltp\layer\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_NoDuplicateID\jhenwnoorlander_jhenwnoorlander_2onder_201405_6ad1e64d-9941-4680-875c-d23022addb78_Prod.txt"
%}

%% Get cleaned training data
[data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data();

% plot data before alignment
figure;
ax(1) = subplot(3,1,1);
plot(data.(AGE),data.(PAR),'r.','DisplayName','FE data BEFORE alignment');
xlim([0,max(data.(AGE))])
legend('show')

% Align data to EP=40%
Fcns_get_predictions = LI_SG_Layers_FE_get_predictions();
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    x_EP = data.(AGE)(flock);
    y_EP = data.(EP)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);   
    [y,x,ynorm_new,ynorm_new_days,init_days]=Fcns_get_predictions.align_data(y,x,y_EP,x_EP,ynorm,ynorm_days);
    data.(AGE)(flock) = x;
    data.(PAR)(flock) = y;
end

% remove NaN
data = data(~isnan(data.(PAR)),:);

% plot data after alignment
ax(2) = subplot(3,1,2);
plot(data.(AGE),data.(PAR),'r.','DisplayName','FE data AFTER alignment');
legend('show')

% limit alined FE data to 550 days
days_max = 550;
data = data(data.(AGE) <= days_max,:);
ax(3) = subplot(3,1,3);
plot(data.(AGE),data.(PAR),'r.','DisplayName',['FE data up to ' num2str(days_max) ', N = ' num2str(length(unique(data.(GROUP)))) ' flocks']);
legend('show')
xlabel('Days')
linkaxes(ax,'xy')

%% get slopes
age_min = 100; % Start time (after aligned)

data.slope = nan(size(data,1),1);
data.slope_lower = nan(size(data,1),1);
data.slope_upper = nan(size(data,1),1);
for k = unique(data.(GROUP))'
    k
    flock = strcmp(data.(GROUP),k);
    data_flock = data(flock,:);
    pos = (data_flock.(AGE)>=age_min) & ~isnan(data_flock.(PAR)) & ~isnan(data_flock.(AGE));
    days2years = 1/days(years(1));
    x = data_flock.(AGE)(pos)*days2years;
    y = data_flock.(PAR)(pos);
    X = [ones(size(x)) x];
    [B,BINT] = regress(y,X);
    data.slope(flock) = B(2);
    data.slope_lower(flock) = BINT(2,1);
    data.slope_upper(flock) = BINT(2,2);
    
    % plot cases with give CLIENT and LINE
    CLIENT_select = 'AvibelLeghen';
    LINE_select = 'NovBLiAlt';
    client = data.(CLIENT)(find(flock,1));
    line   = data.line(find(flock,1));
    if strcmpi(client,CLIENT_select) && strcmpi(line,LINE_select)
        figure('Position',[230 250 1200 700])
        plot(data.(AGE)(flock),data.(PAR)(flock),x/days2years,y,'o')
        xlabel('Days (aligned to EP)')
        ylabel('FE (g)')
        title([client{:} ' / ' line{:} ': slope = ' num2str(B(2)) ' (' num2str(BINT(2,:)) ') (g/year, 95% CI)'],'Interpreter','none')
    end
end

% remove duplicate flockIDs
[flockIDs,IA]  = unique(data.(GROUP));
data = data(IA,:);
data.slope_positive = data.slope_lower > 0;
data.slope_negative = data.slope_upper < 0;
data.slope_zero = (data.slope_upper >= 0) & (data.slope_lower <= 0);


%% stats
disp(' ------------- Overall stats ---------------------');
Tstats_overall_slope = grpstats(data,[],{'mean','predci'},'DataVars',{'slope'})
writetable(Tstats_overall_slope)
Tstats_overall_cat = grpstats(data,[],{'mean'},'DataVars',{'slope','slope_positive','slope_zero','slope_negative'})
writetable(Tstats_overall_cat)

disp(' ------------- Stats per client ---------------------')
Tstats_client = grpstats(data,{'client'},{'mean','predci'},'DataVars',{'slope'});
Tstats_client_slope = sortrows(Tstats_client,'client','ascend')
writetable(Tstats_client_slope)
Tstats_client = grpstats(data,{'client'},{'mean'},'DataVars',{'slope','slope_positive','slope_zero','slope_negative'});
Tstats_client_cat = sortrows(Tstats_client,'client','ascend')
writetable(Tstats_client_cat)

disp(' ------------- Stats per line ---------------------')
Tstats_line = grpstats(data,{'line'},{'mean','predci'},'DataVars',{'slope'});
Tstats_line_slope = sortrows(Tstats_line,'line','ascend')
writetable(Tstats_line_slope)
Tstats_line = grpstats(data,{'line'},{'mean'},'DataVars',{'slope','slope_positive','slope_zero','slope_negative'});
Tstats_line_cat = sortrows(Tstats_line,'line','ascend')
writetable(Tstats_line_cat)

disp(' ------------- Stats per client & line ---------------------')
Tstats_client_line = grpstats(data,{'client','line'},{'mean','predci'},'DataVars',{'slope'});
Tstats_client_line_slope = sortrows(Tstats_client_line,'client','ascend')
writetable(Tstats_client_line_slope)
Tstats_client_line = grpstats(data,{'client','line'},{'mean'},'DataVars',{'slope','slope_positive','slope_zero','slope_negative'});
Tstats_client_line_cat = sortrows(Tstats_client_line,'client','ascend')
writetable(Tstats_client_line_cat)

end

%% Get training data
function [data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data()
% GET_TRAINING_DATA reads all PROD files in 'datafolder' and returns table with selected
% parameters, grouping variable, and aliases for frequently used fields in data.

datafolder = '..\..\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_subset_test'
datafolder = '..\..\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_NoDuplicateID' % duplicated flockIDs
datafolder = '..\Data\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_uniqueID_cleanFE'
%{
Params available in LI_SG:
       'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param,
%}

% Aliases for (frequently used) fields in data
PAR = 'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param
NORM = 'FE1';
PAR_DISPLAY_NAME = PAR;
EP = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00';
EP_NORM = 'EP';
AGE = 'age_days';
AGE_DISPLAY_NAME = 'Age (days)';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

data = PRODs2table(datafolder,AGE,{PAR,NORM,EP,EP_NORM},GROUP,CLIENT,DESCRIPTOR);

%%{
% clean data
Fcns_get_predictions = LI_SG_Layers_FE_get_predictions();
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    y = Fcns_get_predictions.clean_data(y,x,ynorm,ynorm_days);
    data.(AGE)(flock) = x;
    data.(PAR)(flock) = y;
end
%} 
% remove NaN
data = data(~isnan(data.(PAR)),:);
end

