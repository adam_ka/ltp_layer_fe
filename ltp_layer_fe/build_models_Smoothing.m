function build_models()
% Build and test linear mixed effects model for layer Feed Intake (FE)
% Use moving mean to smooth:
%   - training data
%   - past data when making predictions
% 
% Bussiness expectations
%  LTP-lay FE [-5g, +5g] after peak (weeks 30-35) horizon 2-3 months 
%
% tested with MATLAB R2016a
%
%{
 TODO:
- sort slides by rms of residuals
- estimate measurement noise
%}

% OUTPUT
model_name = 'LTPLinearMixedModel';

%{
% current model LI_SG v1.3.0
% C = [Intercept(ww),1./(exp(sqrt(ww))),ww,sqrt(ww)]; % this is the needed data format. So with the 4 columns that are used in the linear mixed model.
model_args =  {'design',@(x) [ones(numel(x),1), 1./exp(sqrt(x(:))), x(:), sqrt(x(:))]}
ppt_filename = 'layer_LTP_FE_LI_SG_1.3.0.pptx';
%}

%%{ 
% Lokhorst 1996, Poultry Sci
%model_args =  {'design',@(x) [121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) -0.028*x(:) 1.617e-5*x(:).^2]} % Lokhorst 1996, Poultry Sci
% Lokhorst - customized 
model_args =  {'design',@(x) [ones(numel(x),1), 121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))), 121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-225)/5))]}
%model_args =  {'design',@(x) [ones(numel(x),1), 121.513./(1+0.434*exp(-121.513*4.027e-4*x(:)))]}

ppt_filename = 'layer_LTP_FE_Lokhorst_modified_smoothing.pptx';
%}

model_version = 'test'; %datestr(now(),'yyyy-mm-dd');
model_file = ['FE_layer_all_' model_version '.mat'];

% OPTIONS
max_age_days = 490 % (=70*7) after alignment to EP (i.e., shifter by 19 weeks (133 days) on average 
                    % Norm max = 650 days (93 weeks)
                    % Norm max = 518 days (74 weeks) after alignment
                    
max_training_days = inf; % max num of days from age_today into the past used for flock prediction
min_fit_range = 7*0; % min num of days from age_today into the past used for single flock prediction
min_obs = 6; % This is a technical limitation of the model fitting, so should be moved there?

Age_today = [61 44 28 11]*7; % after alignment to EP (i.e., shifted by 19 weeks (133 days) on average

Line_color = {'r','g','b','m'};
xlim = [0 max_age_days];

% Initialize PPTX export
if exist(ppt_filename,'file')
    delete(ppt_filename);
end % if

%% Start new presentation
try
    isOpen = exportToPPTX();
catch % add exportToPPTX toolbox to path
    addpath('exportToPPTX');
    isOpen = exportToPPTX();
end % try
    
if ~isempty(isOpen),
    % If PowerPoint already started, then close first and then open a new one
    exportToPPTX('close');
end
try
    exportToPPTX('open',fileName_str);
catch
    exportToPPTX('new','Dimensions',[11 8.5]);
end

%% Get cleaned training data 
%[data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data();
%save('..\data\data_temp','data','AGE','AGE_DISPLAY_NAME','PAR','NORM','PAR_DISPLAY_NAME','EP','EP_NORM','GROUP','CLIENT','DESCRIPTOR')
load('..\data\data_temp')
%data = data(1:20000,:);

% plot cleaned data before alignment
h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
plot(data.(AGE),data.(PAR),'r.','DisplayName','Cleaned data before alignment');
xlabel('Age (Days)')
ylabel(PAR,'Interpreter','none')
legend('show')
slideNum = exportToPPTX('addslide');
exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
exportToPPTX('addtext','Training dataset');
exportToPPTX('addnote',mfilename);

% Align data to EP=40%
Fcns_get_predictions = LI_SG_Layers_FE_get_predictions();
shift_days = [];
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    x_EP = data.(AGE)(flock);
    y_EP = data.(EP)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);   
    [y,x,ynorm_new,ynorm_new_days,init_days]=Fcns_get_predictions.align_data(y,x,y_EP,x_EP,ynorm,ynorm_days);
    shift_days(end+1) = init_days;
    data.(AGE)(flock) = x;
    data.(PAR)(flock) = y;
end
disp(['Mean shift by ' num2str(nanmean(init_days)) ' days'])
%} 
% remove NaN
data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);

% remove data after max_age_days
data = data(data.(AGE)<=max_age_days,:);

% Check flock length and data coverage
age_days_min = 350; %=50*7
n_data_min = 5;
binEdges = linspace(0,age_days_min,n_data_min+1);
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    if ~all(histcounts(x,binEdges))
        data.(AGE)(flock) = nan;
        data.(PAR)(flock) = nan;
    end
end
% remove NaN
data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);

% Check flock properties
% Select only flocks that pass both MM and PN checks
% load initial MM model, to be used for flock checking (selection) before training the
% final MM model
model_struct_0 = load('FE_layer_all_test_20210210 layer_LTP_FE_Lokhorst_225_5.mat');
model_0 = LTPModel.from_struct(model_struct_0);
disp('Checking flock properties ...')
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);   
    pred = Fcns_get_predictions.check_flock_properties(y,x,model_0,ynorm,ynorm_days);
    if ~(pred(2) && pred(1))
        data.(AGE)(flock) = nan;
        data.(PAR)(flock) = nan;
    end
end
% remove NaN
data = data(~isnan(data.(PAR)) & ~isnan(data.(AGE)),:);

% Get 2nd data set with norms
data2 = data;
PAR2 = NORM;
NORM2 = NORM;
PAR2_DISPLAY_NAME = 'Norm FE1';

%% Adjust multiphasic model params
%{
mp_model = @(PHI,t) PHI(1)*exp(-exp(-PHI(2)*(t/7-PHI(3)/7))) + PHI(4)*exp(-exp(-PHI(5)*(t/7-PHI(6)/7))) %+ PHI(7) not converging with constant term
PHI0 = [1000 0.0879 0 1000 0.0879*3 130];
[group_name,group_start,group] = unique(data.(GROUP));
PHI1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,PHI0,'RefineBeta0',false) 
%}

%{
mp_model = @(P,x) P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(2)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(3))/P(4))) + P(5);
P0 = [0.6444 -0.0357 225 5 45.14];

mp_model = @(P,x) P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(2)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(3))/5)) + P(4);
P0 = [0.6444 -0.0357 225 45.14];

mp_model = @(P,x) 0.6444*121.513./(1+0.434*exp(-121.513*4.027e-4*x(:))) + P(1)*121.513./(1+0.434*exp(-121.513*4.027e-4*(x(:)-P(2))/5)) + 45.14;
P0 = [-0.0357 225];
[group_name,group_start,group] = unique(data.(GROUP));
P1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,P0,'RefineBeta0',false) 

'RefineBeta0',true

'RefineBeta0',false

%}

%% Train model
% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));

model = feval(model_name,model_args{:});

%%{
% comment this section to disable smoothing
% smooth data for training
window_length = 49; %[days] 49(default), 28
data.PARmovmean = nan(size(data,1),1);
for k = unique(data.(GROUP))'
    flock = strcmp(data.(GROUP),k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    xs = nan(max(x),1);
    xs(x)=y;
    %xs = movmean(xs,window_length,'omitnan','EndPoints','shrink');
    xs = movmedian(xs,window_length,'omitnan','EndPoints','shrink');
    data.PARmovmean(flock) = xs(x);
end
data.([PAR 'unsmoothed']) = data.(PAR);
data.(PAR) = data.PARmovmean;
%}
[model,ds] = model.fit(data.(AGE),data.(PAR),group);
model.save(model_file);
clear model
model_struct = load(model_file);
model = LTPModel.from_struct(model_struct);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
line(ds.x,ds.y,'marker','+','linestyle','none','DisplayName',['data, N=' num2str(length(group_name)) ' flocks']);
line(ds.x,ds.yhat,'marker','o','linestyle','none','color','g','DisplayName','fit with random effects');
line(ds.x,ds.yhat_fixed,'marker','*','linestyle','none','color','r','DisplayName','fit with fixed effects only');
title(['Training ' func2str(model.design)],'interpreter','none');
ylabel(PAR_DISPLAY_NAME,'interpreter','none')
xlabel(AGE,'interpreter','none')
legend('show');
set(gca,'xgrid','on','ygrid','on');
%ylim = [min([ds.y; ds.yhat; ds.yhat_fixed]) max([ds.y; ds.yhat; ds.yhat_fixed])];
%set(gca,'xlim',xlim,'ylim',ylim,'xgrid','on','ygrid','on');

slideNum = exportToPPTX('addslide');
fprintf('Added slide %d\n',slideNum);
exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
exportToPPTX('addtext',['Linear Mixed Model training with smoothed data, moving window = ' num2str(window_length)]);
exportToPPTX('addnote','');

%% Test model with 1st dataset
data = testing( model,...
                data,...
                [],...
                AGE,AGE_DISPLAY_NAME,PAR,NORM, PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);
%%{
% comment this section to disable smoothing
data.(PAR) = data.([PAR 'unsmoothed']);
%}          
prediction_performance_summary( data,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                max_training_days,Line_color,xlim,...
                                model)
                           
%% Testing on 2nd dataset
%{
data2 = testing(model,...
                data2,...
                LI_SG_BBR_BW_get_predictions(),...
                AGE,AGE_DISPLAY_NAME,PAR2, NORM2, PAR2_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);

prediction_performance_summary( data2,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR2,PAR2_DISPLAY_NAME,...
                                max_training_days,Line_color,xlim,...
                                model)
%}

%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
newFile = exportToPPTX('saveandclose',ppt_filename);

end

%% Get training data
function [data,AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME,EP,EP_NORM,GROUP,CLIENT,DESCRIPTOR] = get_training_data()
% GET_TRAINING_DATA reads PROD files and returns table with selected
% parameters, grouping variable, and aliases for frequently used fields in data.

%datafolder = '..\Data\PredictionPerformanceData_20210122\layers_inactive_weekly_filtered_uniqueID_cleanFE'
datafolder = '..\Data\layers_with_norms_old_and_new_param_names'

%{
Params available in LI_SG:
       'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param,
%}

% Aliases for (frequently used) fields in data
PAR = 'lay_fecpl_fa00_hoor_co_fe_fpwwav_00'; % Feed Intake, weekly param
PAR_old = 'feed_hen_week';
NORM = 'FE1';
PAR_DISPLAY_NAME = PAR;
EP = 'lay_cgalc_fa00_hoor_00_00_fpwwav_00';
EP_old = 'lay_percentage_week';
EP_NORM = 'EP';
AGE = 'age_days';
AGE_DISPLAY_NAME = 'Age (days)';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

data = PRODs2table(datafolder,AGE,{PAR,PAR_old,NORM,EP,EP_old,EP_NORM},GROUP,CLIENT,DESCRIPTOR);

%Combine old and new parameter into new 
% Select the parameter with larger number of valid data, if both old and
% new params have data
disp('Merging data from old- and new-style parameters ... ')
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    y_old = data.(PAR_old)(flock);
    N_old = sum(~isnan(y_old));
    if N_old > 0
        y_new = data.(PAR)(flock);
        N_new = sum(~isnan(y_new));
        if N_old > N_new
            data.(PAR)(flock) = y_old;
            data.(EP)(flock) = data.(EP_old)(flock); 
        end
    end
end

disp('Removing flocks with duplicate or NaN Age values ...')
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    x = data.(AGE)(flock);
    x_diff_unique = unique(diff(x));
    if length(x_diff_unique)~=1
        data(flock,:)=[];
    end
end

%%{
% clean data
disp(['Cleaning the ' PAR ' data ...'])
Fcns_get_predictions = LI_SG_Layers_FE_get_predictions();
for k = unique(data.filename)'
    flock = strcmp(data.filename,k);
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    y = Fcns_get_predictions.clean_data(y,x,ynorm,ynorm_days);
    data.(AGE)(flock) = x;
    data.(PAR)(flock) = y;
end
%} 

% Find Prod files with duplicate IDs
duplicate_id = {};
for id = unique(data.(GROUP))'
    pos = strcmp(id,data.(GROUP));
    files = unique(data.filename(pos,:));
    if length(files) > 1
        duplicate_id{end+1} = files;
    end
end

% For each set of duplicate flock IDs, find files with least data for a give
% parameter.
for i = 1:length(duplicate_id)
    files = duplicate_id{i};
    for j = 1:length(files)
        f = files{j};
        pos = strcmp(f,data.filename);
        y = data.(PAR)(pos);
        duplicate_id_dataCount(i,j) = sum(~isnan(y));
    end
    [~,imax] = max(duplicate_id_dataCount(i,:));
    duplicate_files_to_remove{i} = files(setdiff(1:end,imax));
end
disp('Removing duplicate files with least data ...')
duplicate_files_to_remove = [duplicate_files_to_remove{:,:}]';
for i = 1:length(duplicate_files_to_remove)
    f = duplicate_files_to_remove{i};
    pos = strcmp(f,data.filename);
    data(pos,:) = [];
end

% remove NaN
pos = isnan(data.(PAR));
data = data(~pos,:);
end

%%  plot prediction performance summary
function prediction_performance_summary(data,...
                                        Age_today,AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                        max_training_days,Line_color,xlim,...
                                        model)
for p = 1:4
    h_fig(p) = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    h_ax(p) = axes();
end % for
for j = 1:length(Age_today)
    ageToday = Age_today(j);
    try
        pred_error = data.([PAR '_pred_' num2str(j)]) - data.(PAR);
    catch
        continue
    end % try
    in = isfinite(pred_error);
    [age,~,age_grp] = unique(data.(AGE)(in));
    mean_measure = grpstats(data.(PAR)(in),age_grp,@mean);
    [mean_pred_error,std_pred_error,num_pred,mean_abs_error] = grpstats(pred_error(in),age_grp,{'mean','std','numel',@(x) mean(abs(x))});
    disp_name = ['Fit=[' num2str(ageToday-max_training_days) ',' num2str(ageToday) '['];
    LineWidth = 2;
    MarkerSize = 8;
    line(h_ax(1),age,mean_measure,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' mean']);
    line(h_ax(2),age,mean_pred_error,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' error mean']);
    patch(h_ax(2),[age;flipud(age)],[mean_pred_error+std_pred_error;flipud(mean_pred_error-std_pred_error)],...
          Line_color{j},'DisplayName',[disp_name ' error std'],'EdgeColor','none','FaceAlpha',0.1);
    line(h_ax(3),age,num_pred,      'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' num']);
    line(h_ax(4),age,mean_abs_error,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' MAE']);
end % for
for p = 1:4
    xlabel(h_ax(p),AGE_DISPLAY_NAME);
    title(h_ax(p),['PARAMETER = ' PAR_DISPLAY_NAME ', MODEL = ' class(model) ': ' func2str(model.design)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
end % for
ylabel(h_ax(1),PAR_DISPLAY_NAME,'interpreter','none');
ylabel(h_ax(2),['Prediction Error = Pred - ' PAR_DISPLAY_NAME],'interpreter','none');
%set(h_ax(2),'ylim',[-20 20]);
ylabel(h_ax(3),'Number of flocks');
ylabel(h_ax(4),'Mean Absolute Error');
fig_text = {'Average measurement','Prediction error distribution','Number of data points','Mean Absolute Error'};
for p = 1:4
    slideNum = exportToPPTX('addslide');
    fprintf('Added slide %d\n',slideNum);
    exportToPPTX('addpicture',h_fig(p),'Scale','maxfixed');
    exportToPPTX('addtext',fig_text{p});
    exportToPPTX('addnote','');
end % for

end

%%
function data = testing(model,...
                        data,...
                        Fcns_get_predictions,...
                        AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                        max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim)
%model = feval(lme.model_cmd,lme.model_args{:});
%model_name = lme.model_name;

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
client_name = data.(CLIENT)(group_start);
descriptor = data.(DESCRIPTOR)(group_start);
num_units = numel(group_name);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
for j = 1:length(Age_today)
    data.([PAR '_pred_' num2str(j)]) = nan(size(data,1),1);
end % for

for k = 1:num_units
    %flock = group==k & isfinite(data.(PAR)) & data.(PAR)>=0 & data.(PAR)<100 & data.(AGE)<=max_age_days;
    flock = group==k & isfinite(data.(PAR)) & data.(AGE)<=max_age_days;
    if ~any(flock)
        continue
    end
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    y_unsmoothed = data.([PAR 'unsmoothed'])(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    min_x = min(x);
    max_x = max(x);

    has_pred = false;    
    clf(h_fig);
    h_ax = axes();
    line(x,y_unsmoothed,'linestyle','none','marker','*','MarkerSize',8,'color','k','DisplayName','Data');
    line(x,y,'linestyle',':','marker','.','MarkerSize',8,'color','k','DisplayName','Data - moving mean');
    for j = find(Age_today>min_x+min_fit_range & Age_today<=max_x)
        ageToday = Age_today(j);
        fit_range = [max(min_x,ageToday-max_training_days) ageToday];
        fit = x>=fit_range(1) & x<fit_range(2);
        x_fit = x(fit);
        if numel(x_fit)<min_obs
            continue
        end % if
        has_pred = true;
        y_fit = y(fit);  
        
        prd = x>=ageToday;
        yhat = nan(size(y));
        
        if ~isempty(Fcns_get_predictions)
            % clean data and check flock properties
            y_fit = Fcns_get_predictions.clean_data(y_fit,x_fit,ynorm,ynorm_days);
            pos = ~isnan(y_fit);
            x_fit = x_fit(pos);
            y_fit = y_fit(pos);
            pred = Fcns_get_predictions.check_flock_properties(y_fit,x_fit,model);
            if pred(1)==0
                continue
            end
        end
        yhat(prd) = model.predict(x(prd),x_fit,y_fit);
        data.([PAR '_pred_' num2str(j)])(flock) = yhat;

        disp_name = ['Fit=[' num2str(fit_range(1)) ',' num2str(fit_range(2)) '['];
        data_disp_name = [disp_name ' Data'];
        pred_disp_name = [disp_name ' Pred'];
        p = 1; %:2 % 1 = full prediction, 2 = after ageToday only
        %line(h_ax(p),x_fit,y_fit,'linestyle','none','marker','*','color',Line_color{j},'DisplayName',data_disp_name);
        LineWidth = 2;
        % show future predictions beyond available data
        line(h_ax(p),max_x:max_age_days,model.predict(max_x:max_age_days,x_fit,y_fit),'linestyle',':','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
        % show future predictions within available data
        line(h_ax(p),fit_range(2):max_x,model.predict(fit_range(2):max_x,x_fit,y_fit),'linestyle','-','LineWidth',LineWidth,'marker','none','color',Line_color{j},'DisplayName',pred_disp_name);
        % show model fitting to the data up to age today
        line(h_ax(1),fit_range(1):fit_range(2),model.predict(fit_range(1):fit_range(2),x_fit,y_fit),'linestyle','--','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); 
    end % for
    ylim = [-5 5*(1+ceil(max(y)/5))];
    p = 1;%:2
    xlabel(h_ax(p),AGE_DISPLAY_NAME)
    ylabel(h_ax(p),PAR_DISPLAY_NAME,'interpreter','none')
    title(h_ax(p),[PAR_DISPLAY_NAME ' model = ' class(model)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'ylim',ylim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
    if has_pred
        slideNum = exportToPPTX('addslide');
        fprintf('Added slide %d\n',slideNum);
        exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
        exportToPPTX('addtext',['CLIENT:' client_name{k} ' UNIT:' group_name{k} ' DESCRIPTOR:' descriptor{k} ' MODEL:' class(model)]);
        exportToPPTX('addnote','');
    end % if
end % for
end
